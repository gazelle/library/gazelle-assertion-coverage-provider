package net.ihe.gazelle.assertion.coverage.provider.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Wraps assertions into an xml element
 */
@XmlRootElement(name = "assertions")
public class WsAssertionWrapper {

    /**
     * Field assertions.
     */
    @XmlElement(name = "assertion")
    public List<WsAssertion> assertions = new ArrayList<WsAssertion>();

    /**
     * Constructor for WsAssertionWrapper.
     */
    public WsAssertionWrapper() {
    }

    /**
     * Constructor for WsAssertionWrapper.
     *
     * @param listDistinct List<WsAssertion>
     */
    public WsAssertionWrapper(List<WsAssertion> listDistinct) {
        Collections.sort(listDistinct);
        assertions = listDistinct;
    }

    /**
     * Method getAssertions.
     *
     * @return List<WsAssertion>
     */
    public List<WsAssertion> getAssertions() {
        return assertions;
    }
}
