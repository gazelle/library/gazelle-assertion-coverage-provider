package net.ihe.gazelle.assertion.coverage.provider.data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * WsAssertion contains an assertion and a list of entities covering her
 */
@XmlRootElement(name = "wsAssertion")
public class WsAssertion implements Comparable<WsAssertion> {

    /**
     * Field assertionId.
     */
    public String assertionId;
    /**
     * Field idScheme.
     */
    public String idScheme;

    /**
     * Field coveringEntities.
     */
    public List<CoveringEntity> coveringEntities;

    /**
     * Constructor for WsAssertion.
     */
    public WsAssertion() {
    }

    /**
     * Constructor for WsAssertion.
     *
     * @param idScheme         String
     * @param assertionId      String
     * @param description      String
     * @param coveringEntities List<CoveringEntity>
     */
    public WsAssertion(String idScheme, String assertionId, List<CoveringEntity> coveringEntities) {
        this.assertionId = assertionId;
        this.idScheme = idScheme;
        this.coveringEntities = coveringEntities;
    }

    /**
     * Method getAssertionId.
     *
     * @return String
     */
    public String getAssertionId() {
        return assertionId;
    }

    /**
     * Method getIdScheme.
     *
     * @return String
     */
    public String getIdScheme() {
        return idScheme;
    }

    /**
     * Method compareTo.
     *
     * @param o WsAssertion
     * @return int
     */
    @Override
    public int compareTo(WsAssertion o) {
        if (o == null) {
            return 1;
        }
        return this.assertionId.compareTo(o.assertionId);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        WsAssertion other = (WsAssertion) object;
        return this.assertionId.equals(other.getAssertionId()) && this.idScheme.equals(other.getIdScheme());
    }

    /**
     * Method getCoveringEntities.
     *
     * @return List<CoveringEntity>
     */
    public List<CoveringEntity> getCoveringEntities() {
        return coveringEntities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = assertionId != null ? assertionId.hashCode() : 0;
        result = prime * result + (idScheme != null ? idScheme.hashCode() : 0);
        return result;
    }
}