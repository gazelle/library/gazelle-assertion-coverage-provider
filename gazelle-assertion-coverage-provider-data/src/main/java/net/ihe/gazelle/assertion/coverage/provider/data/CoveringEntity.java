package net.ihe.gazelle.assertion.coverage.provider.data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Covering Entity is used to provide a link to a rule that covers an assertion
 */
@XmlRootElement(name = "coveringEntity")
public class CoveringEntity implements Comparable<CoveringEntity> {

    public CoveringEntity() {
        super();
    }

    /**
     * Constructor for CoveringEntity.
     *
     * @param keyword       String
     * @param permanentLink String
     */
    public CoveringEntity(String keyword, String permanentLink) {
        super();
        this.keyword = keyword;
        this.permanentLink = permanentLink;
    }

    /**
     * Field keyword.
     */
    public String keyword;
    /**
     * Field permanentLink.
     */
    public String permanentLink;

    public void setBaseUrl(String baseUrl) {
        this.permanentLink = baseUrl + permanentLink;
    }

    /**
     * Method getKeyword.
     *
     * @return String
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Method getPermanentLink.
     *
     * @return String
     */
    public String getPermanentLink() {
        return permanentLink;
    }

    /**
     * Method compareTo.
     *
     * @param o CoveringEntity
     * @return int
     */
    @Override
    public int compareTo(CoveringEntity o) {
        if (o == null) {
            return 1;
        }
        return this.keyword.compareTo(o.keyword);
    }

}
