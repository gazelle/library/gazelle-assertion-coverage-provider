package net.ihe.gazelle.assertion;

import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertionWrapper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WsAssertionWrapperTest {

    @Test
    public void test() {
        WsAssertion assertion = new WsAssertion("IdScheme", "CONF-1", null);

        List<WsAssertion> list = new ArrayList<WsAssertion>();
        list.add(assertion);

        WsAssertionWrapper wrapper = new WsAssertionWrapper(list);
        assertEquals(1, wrapper.getAssertions().size());
        assertEquals("CONF-1", wrapper.getAssertions().get(0).getAssertionId());
    }

}
