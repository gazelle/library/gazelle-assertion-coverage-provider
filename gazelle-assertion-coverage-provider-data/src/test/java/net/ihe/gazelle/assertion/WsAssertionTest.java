package net.ihe.gazelle.assertion;

import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WsAssertionTest {

    @Test
    public void test() {
        WsAssertion assertion = new WsAssertion("IdScheme", "CONF-1", null);
        assertEquals("IdScheme", assertion.getIdScheme());
        assertEquals("CONF-1", assertion.getAssertionId());
    }

}
