package net.ihe.gazelle.assertion.coverage.provider.client;

import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertionWrapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoverageProviderClientTest {

    @Test
    public void getAssertionsCoveredByService() {
        CoverageProviderClient client = new CoverageProviderClient();
        WsAssertionWrapper assertionsWrapper = client.getTestAssertionsCovered();
        assertEquals(1, assertionsWrapper.getAssertions().size());
    }

    @Test
    public void getTestAssertionCoverageByIdScheme() {
        CoverageProviderClient client = new CoverageProviderClient();
        WsAssertionWrapper assertionsWrapper = client.getTestAssertionCoverageByIdScheme("CLO");
        assertEquals(0, assertionsWrapper.getAssertions().size());
    }

    @Test
    public void getTestAssertionCoverageWithBadIdScheme() {
        CoverageProviderClient client = new CoverageProviderClient();
        WsAssertionWrapper assertionsWrapper = client.getTestAssertionCoverageByIdScheme("CLOCLO");
        assertEquals(0, assertionsWrapper.getAssertions().size());
    }

    @Test
    public void getAssertionsForBadTestStep() {
        CoverageProviderClient client = new CoverageProviderClient();
        WsAssertion assertionWrapper = client.getATestAssertionCoverage("CLO", "CLO-070");
        assertEquals("CLO-070", assertionWrapper.getAssertionId());
    }

}
