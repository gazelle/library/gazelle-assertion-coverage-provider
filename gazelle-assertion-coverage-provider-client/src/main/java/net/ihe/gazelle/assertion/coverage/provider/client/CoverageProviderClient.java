package net.ihe.gazelle.assertion.coverage.provider.client;

import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertionWrapper;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoverageProviderClient {

    private static Logger log = LoggerFactory.getLogger(CoverageProviderClient.class);
    private String baseUrl;

    public CoverageProviderClient() {
        setBaseUrl("https://k-project.ihe-europe.net/XDStarClient");
    }

    public CoverageProviderClient(String baseUrl) {
        setBaseUrl(baseUrl);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl + "/rest";
    }

    public WsAssertionWrapper getTestAssertionsCovered() {
        log.info("getTestAssertionsCovered");
        AssertionsRequest request = new AssertionsRequest(getBaseUrl());
        request.setPathParameters("/coverage/all");
        ClientResponse<String> response = request.performRequest();

        return response.getEntity(WsAssertionWrapper.class);
    }

    public WsAssertionWrapper getTestAssertionCoverageByIdScheme(String idScheme) {
        log.info("getTestAssertionCoverageByIdScheme");
        AssertionsRequest request = new AssertionsRequest(getBaseUrl());
        request.setPathParameters("/coverage/idScheme/" + idScheme);
        ClientResponse<String> response = request.performRequest();

        return response.getEntity(WsAssertionWrapper.class);
    }

    public WsAssertion getATestAssertionCoverage(String idScheme, String assertionId) {
        log.info("getATestAssertionCoverage");
        AssertionsRequest request = new AssertionsRequest(getBaseUrl());
        request.setPathParameters("/coverage?assertionId=" + assertionId + "&idScheme=" + idScheme);
        ClientResponse<String> response = request.performRequest();

        return response.getEntity(WsAssertion.class);
    }
}
