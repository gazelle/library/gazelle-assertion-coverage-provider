package net.ihe.gazelle.assertion.coverage.provider.client;

import org.apache.commons.httpclient.HttpStatus;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Request {

    private static Logger log = LoggerFactory.getLogger(Request.class);

    private String baseUrl;

    private String pathParameters = "";

    public Request(String baseUrl) {
        super();
        this.baseUrl = baseUrl;
    }

    public ClientResponse<String> performRequest() {
        ClientResponse<String> response = new NullClientResponse();
        try {
            String url = baseUrl + getResourcePath() + pathParameters;

            log.info("request on: " + url);
            ClientRequest request = new ClientRequest(url);
            request.accept("text/xml");
            response = request.get(String.class);

            if (response.getStatus() != HttpStatus.SC_OK && response.getStatus() != HttpStatus.SC_NO_CONTENT) {
                response = new NullClientResponse();
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

        } catch (java.net.ConnectException e) {
            log.error("cannot reach host: " + baseUrl);

        } catch (Exception e) {
            log.error("cannot reach parse response " + baseUrl + getResourcePath() + pathParameters);
        }
        return response;
    }

    public abstract String getResourcePath();

    public void setPathParameters(String params) {
        pathParameters = params;
    }
}
