package net.ihe.gazelle.assertion.coverage.provider.client;

import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.spi.Link;
import org.jboss.resteasy.spi.LinkHeader;
import org.jboss.resteasy.util.GenericType;

import javax.ws.rs.core.MultivaluedMap;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Map;

public class NullClientResponse extends ClientResponse<String> {
    private static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(NullClientResponse.class);

    @Override
    public MultivaluedMap<String, String> getHeaders() {
        return null;
    }

    @Override
    public Status getResponseStatus() {
        return Status.NOT_FOUND;
    }

    @Override
    public String getEntity() {
        return "";
    }

    @Override
    public <T> T getEntity(Class<T> type) {
        T newInstance = null;
        try {
            newInstance = type.newInstance();
        } catch (InstantiationException e) {
            log.error(e.getMessage());
        } catch (IllegalAccessException e) {
            log.error(e.getMessage());
        }
        return newInstance;
    }

    @Override
    public <T> T getEntity(Class<T> type, Type genericType) {
        return null;
    }

    @Override
    public <T> T getEntity(Class<T> type, Type genericType, Annotation[] annotations) {
        return null;
    }

    @Override
    public <T> T getEntity(GenericType<T> type) {
        return null;
    }

    @Override
    public <T> T getEntity(GenericType<T> type, Annotation[] annotations) {
        return null;
    }

    @Override
    public LinkHeader getLinkHeader() {
        return null;
    }

    @Override
    public Link getLocation() {
        return null;
    }

    @Override
    public void releaseConnection() {
    }

    @Override
    public int getStatus() {
        return 0;
    }

    @Override
    public MultivaluedMap<String, Object> getMetadata() {
        return null;
    }

    @Override
    public Link getHeaderAsLink(String headerName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void resetStream() {
        // TODO Auto-generated method stub

    }

    @Override
    public Map<String, Object> getAttributes() {
        // TODO Auto-generated method stub
        return null;
    }
}