package net.ihe.gazelle.assertion.coverage.provider.client;

public class AssertionsRequest extends Request {

    public AssertionsRequest(String baseUrl) {
        super(baseUrl);
    }

    @Override
    public String getResourcePath() {
        return "/testAssertion";
    }

}
