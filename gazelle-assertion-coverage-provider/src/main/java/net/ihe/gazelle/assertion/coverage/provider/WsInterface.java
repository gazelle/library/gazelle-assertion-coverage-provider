package net.ihe.gazelle.assertion.coverage.provider;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.coverage.provider.data.WsAssertionWrapper;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.xml.bind.JAXBException;

@Local
@Path("/testAssertion")
@Api("/testAssertion")
public interface WsInterface {

    @GET
    @Path("/coverage/all")
    @Produces("text/xml")
    @ApiOperation(value = "list all covered assertions", notes = "list all covered assertions", responseClass = "net.ihe.gazelle.assertion.WsAssertionWrapper")
    @Wrapped
    WsAssertionWrapper getTestAssertionsCovered() throws JAXBException;

    @GET
    @Path("/coverage/idScheme")
    @Produces("text/xml")
    @ApiOperation(value = "list all covered assertions in the idScheme", notes = "list all covered assertions in the idScheme", responseClass = "net.ihe.gazelle.assertion.WsAssertionWrapper")
    @Wrapped
    WsAssertionWrapper getTestAssertionCoverageByIdScheme(
            @ApiParam(value = "The idScheme", required = true) @QueryParam("idScheme") String idScheme)
            throws JAXBException;

    @GET
    @Path("/coverage")
    @Produces("text/xml")
    @ApiOperation(value = "return one assertion and the backlink to entities covering her", notes = "return one assertion and the backlink to entities covering her", responseClass = "net.ihe.gazelle.assertion.WsAssertion")
    @Wrapped
    WsAssertion getATestAssertionCoverage(
            @ApiParam(value = "The idScheme", required = true) @QueryParam("idScheme") String idScheme,
            @ApiParam(value = "The assertion id ", required = true) @QueryParam("assertionId") String assertionId)
            throws JAXBException;
}
